
#pragma once


#include <functional>


namespace filesystem {

    enum class EntryType
    {
        Unknown = 0,
        Folder = 1,
        File = 2,
    };

    enum class AccessRights
    {
        None = 0x0,
        Read = 0x1,
        Write = 0x2,
        All = 0x3,
    };

    class EntryFacade;
    class FolderFacade;
    class FileFacade;

    class SystemFacade final
    {
    public:

        FolderFacade createFolder(const std::string& entryPath, AccessRights accessRights);

        FileFacade createFile(const std::string& entryPath, AccessRights accessRights, const std::string& contents);

        EntryFacade findEntry(const std::string& entryPath) const;

        bool deleteEntry(const std::string& entryPath);
    };

    class EntryFacade
    {
    public:

        explicit operator bool() const;

        EntryType getType() const;

        FolderFacade getParent() const;

        std::string getPath() const;

        AccessRights getAccessRights() const;
    };

    class FolderFacade final : public EntryFacade
    {
    public:

        using ChildVisitor = std::function<void(EntryFacade)>;

        unsigned int getChildrenCount() const;

        void visitChildren(ChildVisitor childVisitor) const;
    };

    class FileFacade final : public EntryFacade
    {
    public:

        std::string getContents() const;
    };

    template<typename FacadeT>
    FacadeT facade_cast(const EntryFacade& entryFacade);

} // namespace filesystem
