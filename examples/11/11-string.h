
#pragma once


#include <cstddef>


class String
{
public:

    String();

    String(String&&);

    String(const String& s);

    String(const char* s);

    ~String();

    String& operator=(String&& s);

    String& operator=(const String& s);

    String& operator=(const char* s);

    void swap(String& s);

    const char* getChars() const;

    std::size_t getLength() const;
};

bool operator==(const String& s1, const String& s2);

bool operator!=(const String& s1, const String& s2);

bool operator<(const String& s1, const String& s2);
