
#pragma once


#include <iostream>
#include <string>
#include <vector>


class Shape
{
public:

    virtual ~Shape() = default;

    virtual void draw() const = 0;
};

class Square; // TODO
class Triangle; // TODO
class Circle; // TODO

class ShapeFactory
{
public:

    // TODO
    // ???? createShape(const std::string& shapeType) const;

private:
};

class ShapeManager
{
public:

    void menuLoop()
    {
        std::string command;
        do
        {
            std::cout << "Choose a command\n";
            std::cout << "Type 'c' + ENTER for creating a shape\n";
            std::cout << "Type 'd' + ENTER for displaying the list of shapes\n";
            std::cout << "Type 'q' + ENTER for leaving\n";
            std::cout << "Choice ? ";
            std::cin >> command;

            if (command == "c")
                createShape();
            else if (command == "d")
                drawShapes();

        } while (command != "q");
    }

private:

    void createShape()
    {
        std::string shapeType;
        std::cout << "Which shape type would you like to create ? ";
        std::cin >> shapeType;

        // TODO
        // auto shape = m_shapeFactory.createShape(shapeType);
        // add shape into m_shapes
    }

    void drawShapes()
    {
        std::cout << "List of shapes:\n";

        // TODO
        // for each shape in m_shapes
        //    draw shape
    }

    // TODO
    // std::vector<????> m_shapes;

    ShapeFactory m_shapeFactory;
};
