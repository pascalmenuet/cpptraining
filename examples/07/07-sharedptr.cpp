#include <memory>
#include <cassert>

using namespace std;

void test_sharedptr()
{
    weak_ptr<int> wp2{};
    {
        auto sp1 = make_shared<int>(42);

        weak_ptr<int> wp1 = sp1;
        auto sp2 = wp1.lock();
        assert(sp2 != nullptr && *sp2 == 42);

        wp2 = sp1;
        auto sp3 = wp2.lock();
        assert(sp3 != nullptr && *sp3 == 42);
    }
    auto sp4 = wp2.lock();
    assert(sp4 == nullptr);
}
