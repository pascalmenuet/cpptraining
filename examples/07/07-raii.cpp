
#include <cassert>
#include <string>
#include <vector>
#include <memory>

struct Klasse
{
    Klasse(int i, std::string s)
        : m_i(i), m_s(s)
    {}

    int m_i;
    std::string m_s;
};

void using_new() {
    char* s = new char[50]{ "juke" };
    assert(std::strcmp(s, "juke") == 0);
    // ... use s
    delete [] s; // do not forget it

    int* arr = new int[3]{ 1,2,3 };
    assert(arr[0] == 1 && arr[1] == 2 && arr[2] == 3);
    // ... use arr
    delete [] arr; // do not forget it

    Klasse* pK = new Klasse(42, "buzz");
    assert(pK->m_i == 42 && pK->m_s == "buzz");
    // ... use pK
    delete pK; // do not forget it
}

void using_raii() {
    std::string s("juke");
    assert(s == "juke");
    // ... use s

    std::vector<int> arr{ 1, 2, 3 };
    assert((arr == std::vector<int>{1, 2, 3}));
    // ... use arr

    auto upK = std::make_unique<Klasse>(42, "buzz");
    assert(upK->m_i == 42 && upK->m_s == "buzz");
    // ... use upK
} // destructors of local variables will do the clean up

void test_raii()
{
    using_new();
    using_raii();
}
