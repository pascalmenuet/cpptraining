
#include <exception>
#include <iostream>
#include <memory>

struct C
{
    char* m_p1 = nullptr;
    char* m_p2 = nullptr;

    void create()
    {
        m_p1 = new char;
        try
        {
            m_p2 = new char;
        }
        catch (std::exception& e)
        {
            delete m_p1;
            m_p1 = nullptr;
        }
    }

    void swap(C& c) noexcept
    {
        using std::swap;
        swap(m_p1, c.m_p1);
        swap(m_p2, c.m_p2);
    }

    ~C()
    {
        if (m_p1)
        {
            delete m_p1;
            delete m_p2;
        }
    }

};

void theInnocentWhoLooksGuilty(int val)
{
    if (val == 3)
        throw std::exception("boom");
}

void theGuiltyWhoLooksInnocent()
{
    auto p = std::make_unique<int>(3);
    theInnocentWhoLooksGuilty(*p);
}

void theFooled()
{
    try
    {
        theGuiltyWhoLooksInnocent();
    }
    catch (std::exception& ex)
    {
        std::cout << "there was an exception, but now everything is ok, I swear\n";
    }
}