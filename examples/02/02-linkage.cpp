
// i1 has external linkage
// and is defined in another translation unit
extern int i1;

// i2 has external linkage
// and is defined in this translation unit
int i2;

// i3 has internal linkage
static int i3;

namespace {
    // i4 has internal linkage
    int i4;
    // f has internal linkage
    float f() { return 0.0; }
    // Crotte has internal linkage
    class Crotte {
        // flute has internal linkage
        static long double flute;
    };
}

// nachBerlin has external linkage
void nachBerlin() {
    // b has no linkage
    bool b = false;
    // wc has no linkage
    static wchar_t wc = 0;
}

// Zut has external linkage
class Zut {
    // prout has external linkage
    static char prout;
};
