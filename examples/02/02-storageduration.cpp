
// num and div have automatic duration
unsigned int divide(unsigned int num, unsigned int div) {
    // result has automatic duration
    unsigned int result = 0;

    // pIsZero has automatic duration
    // Object pointed-to by pIsZero has dynamic duration
    bool* pIsZero = new bool(div == 0);

    if (*pIsZero) {
        // specialValue has static duration
        static unsigned int specialValue = 123456789;
        result = specialValue;
    }
    else {
        // result has automatic duration
        result = num / div;
    }

    delete pIsZero;
    return result;
}

// onePerThread has thread duration
thread_local int onePerThread;

class SomeClass {
    // member has automatic duration
    char member;
    // otherMember has static duration
    static double otherMember;
};

// someObj1 has static duration
static SomeClass someObj1;

// someObj2 has also static duration
SomeClass someObj2;
