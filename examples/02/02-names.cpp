
// Deeply nested namespaces
namespace ich { namespace bin { namespace ein { namespace berliner {
    class C {};
} } } }

namespace munchen {
    // Bring all names
    // from namespace ich:bin
    // into namespace munchen
    // (until the next closing bracket)
    using namespace ich::bin;
    namespace {
        // inside an anonymous namespace,
        // c has internal linkage
        ein::berliner::C c;
    }
}

// Provide the alias small
// for the long namespace ich::bin::ein::berliner
namespace small = ich::bin::ein::berliner;
small::C c1;

// Bring the name C
// from namespace small
// into global namespace
using small::C;
C c2;

// Provide the alias MyClass
// for the type small C
using MyClass = C;
MyClass c3;
