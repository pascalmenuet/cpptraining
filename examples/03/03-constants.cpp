
#include <vector>
#include <array>

static double d = 12.415;
const double pi = 3.14;

void func() {
    d = pi; // OK
    // Compilation Error
    // pi = d;
}

class C
{
    double m_d = 1.23;
public:
    double get() const { return m_d; }
    void set(double f) { m_d = d; }
};

void func2()
{
    const C c;
    d = c.get(); // OK

    // Compilation Error
    // c.set(78.59);

    double d = 12.415;
    constexpr double pi = 3.14;

    d = pi; // OK

    // Compilation Error
    // pi = d;
}

// Compilation Error
// constexpr std::vector<int> v = { 1, 2, 3 };

constexpr std::array<int, 3> v = { 1, 2, 3 };

constexpr int mult(int a, int b)
{
    return a * b;
}

// Initialized at compile-time if possible
int six = mult(2, 3);
