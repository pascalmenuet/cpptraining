
#include <string>
#include <iostream>

// Arithmetic global constants
constexpr double pi = 3.14;

// String global constants
const std::string s1 = "qwerty"; // can be problematic
constexpr const char s [] = "qwerty";
//constexpr boost::string_ref s = "qwerty";
//constexpr gsl::string_span = "qwerty";

class F
{
    std::string m_s = s;
};






// The special null value for pointers: nullptr
// The special null type: nullptr_t
class CC
{
};

static void boo()
{
    CC* pC = new (std::nothrow) CC;
    if (pC == nullptr)
        return;
    if (! pC)
        return;
    delete pC;
    pC = nullptr;
    std::nullptr_t pNull = nullptr;
//    pNull = pC;
}




class C
{
public:

    C() = default;

    C(const C&) = default;

    C& operator=(const C&)
    {
        return *this;
    }

    ~C()
    {
    }

    std::string m_s;
};




// Problems with dereferencing pointers:
// null pointers, dangling pointers

struct Type
{
    int m_i;
};

//Type c();

static void fn()
{
    char c [] { 1, 3, 3 };
    {
        Type* pT = new Type { 8 };
        //...
        delete pT;
        // ...
        std::cout << (*pT).m_i; // ouch: dangling pointer
        std::cout << pT->m_i; // ouch
        Type& r = *pT;
        std::cout << r.m_i; // ouch
    }

    {
        Type* pT = new Type{ 8 };
        //...
        delete pT;
        pT = nullptr;
        // ...
        std::cout << (*pT).m_i; // ouch: null pointer
        std::cout << pT->m_i; // ouch
        Type& r = *pT;
        std::cout << r.m_i; // ouch
    }

    Type* pT = nullptr;
    {
        Type t{};
        pT = &t;
        pT = new Type(t);
    }
    std::cout << (*pT).m_i; // ouch: dangling pointer
}








// Returning multiple values with simple struct
struct S
{
    int m_i;
    std::string m_s;
};

static S func()
{
    S s;
    s.m_i = 123;
    s.m_s = "well";
    return s;
}

static void use()
{
    S s = func();
    // ... use s

    const S& r = func();
    // ... use r without fear
}
