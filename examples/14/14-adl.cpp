
namespace foo {
    class Bar {};
    static void func(Bar bar) {}
    static void func(int i) {}
}

void test()
{
    foo::Bar bar;
    // using foo::func; // not needed
    func(bar);

    using foo::func; // needed
    func(8);
}
