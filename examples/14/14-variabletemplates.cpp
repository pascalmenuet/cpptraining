#if defined VARIABLE_TEMPLATES_SUPPORTED_
#include <cassert>
#include <iostream>

template< typename T >
T pi = static_cast<T>(3.141'592'653'589'793'238'462);

void test_variabletemplates()
{
    std::cout << pi<int> << std::endl;
    assert(pi<int> == 3);

    std::cout << pi<float> << std::endl;
    assert(pi<float> == 3.141'592'74f);

    std::cout << pi<double> << std::endl;
    assert(pi<double> == 3.141'592'653'589'793'1);
}
#endif
