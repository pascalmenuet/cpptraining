
#include <iostream>
#include <string>

template< typename OutputStreamT, typename T >
void xmlPrintToStream(OutputStreamT& outputStream, const T& anything, const std::string& xmlElementName)
{
    outputStream << "<" << xmlElementName << ">";
    outputStream << anything;
    outputStream << "</" << xmlElementName << ">\n";
}

template< typename ArgT >
void xmlPrintArg(std::size_t argIndex, ArgT& arg)
{
    std::string xmlElementName = "arg-";
    xmlElementName += std::to_string(argIndex);
    xmlPrintToStream(std::cout, arg, xmlElementName);
}

template< typename ArgT >
void xmlPrintArgsWithIndex(std::size_t argCount, ArgT& arg)
{
    xmlPrintArg(argCount - 1, arg);
}

template< typename ArgT0, typename... ArgTs >
void xmlPrintArgsWithIndex(std::size_t argCount, ArgT0& arg0, const ArgTs&... args)
{
    xmlPrintArg(argCount - sizeof...(args) - 1, arg0);
    xmlPrintArgsWithIndex(argCount, args...);
}

template< typename... ArgTs >
void xmlPrint(const ArgTs&... args)
{
    xmlPrintArgsWithIndex(sizeof...(args), args...);
}

void test_variadictemplates()
{
    xmlPrint(1, 2.3, "hello", true);
}
