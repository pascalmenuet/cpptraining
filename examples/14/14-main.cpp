
void test_functiontemplates();
void test_classtemplates();
void test_variabletemplates();
void test_variadictemplates();
void test_perfectforwarding();
void test_sfinae();

int main()
{
    test_functiontemplates();
    test_classtemplates();
#if defined VARIABLE_TEMPLATES_SUPPORTED_
    test_variabletemplates();
#endif
    test_variadictemplates();
    test_perfectforwarding();
    test_sfinae();
    return 0;
}
