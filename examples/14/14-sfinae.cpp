
#include <type_traits>
#include <cstdio>
#include <cassert>
#include <string>
#include <iostream>

namespace type_traits_test {

    template< typename T >
    struct IsInteger
    {
        static constexpr bool Value = false;
    };

    template<>
    struct IsInteger<int>
    {
        static constexpr bool Value = true;
    };

    template<>
    struct IsInteger<long>
    {
        static constexpr bool Value = true;
    };

    void test()
    {
        {
            const auto result = IsInteger<std::string>::Value;
            assert(!result);
        }

        {
            const auto result = IsInteger<char>::Value;
            assert(!result);
        }

        {
            const auto result = IsInteger<long>::Value;
            assert(result);
        }

        {
            const auto result = IsInteger<int>::Value;
            assert(result);
        }
    }

}

namespace sfinae_test {

    template< typename T >
    typename T::Type1 func(T t)
    {
        printf("The type T declares the inner type Type1 \n");
        return 0;
    }

    template< typename T >
    typename T::Type2 func(T t)
    {
        printf("The type T declares the inner type Type2 \n");
        return 0;
    }

    struct S1
    {
        using Type1 = int;
    };

    struct S2
    {
        using Type2 = char;
    };

    void test()
    {
        func(S1{});
        func(S2{});
    }

}

namespace enable_if_test {

    template<bool B, class T>
    struct enable_if
    {
    };

    template<class T>
    struct enable_if<true, T>
    {
        typedef T type;
    };

    template< bool Condition >
    typename enable_if<Condition, int>::type func()
    {
        printf("func is called with a condition equal to true \n");
        return 42;
    }

    template< bool Condition >
    typename enable_if<! Condition, std::string>::type func()
    {
        printf("func is called with a condition equal to false \n");
        return "42";
    }

    void test()
    {
        {
            const auto result = func<true>();
            assert(result == 42);
        }

        {
            const auto result = func<false>();
            assert(result == "42");
        }
    }

}

namespace enable_if_type_traits_test {

    template< typename T >
    typename std::enable_if<std::is_integral<T>::value, std::string>::type func(T t)
    {
        printf("T is an integral type \n");
        return "integral";
    }

    template< typename T >
    typename std::enable_if<std::is_class<T>::value, int>::type func(const T& t)
    {
        printf("T is a class \n");
        return 42;
    }

    void test()
    {
        {
            const auto result = func(53);
            assert(result == "integral");
        }

        {
            class Toto {};
            const auto result = func(Toto{});
            assert(result == 42);
        }
    }

}

namespace print_test {

    template<typename T>
    struct has_method_print
    {
        template<typename U, void(U::*)() const> struct CHECK_SIGNATURE {};

        template<typename U> static char testFunc(CHECK_SIGNATURE<U, &U::print>*);

        template<typename U> static int testFunc(...);

        static const bool Value = sizeof(testFunc<T>(nullptr)) == sizeof(char);
    };

    template< typename T >
    typename std::enable_if<has_method_print<T>::Value, void>::type print(const T& printableObject)
    {
        std::cout << "Calling printableObject.print(); \n";
        printableObject.print();
        std::cout << "\n";
    }

    template< typename T >
    typename std::enable_if<!has_method_print<T>::Value, void>::type print(const T& printableObject)
    {
        std::cout << "Calling std::cout << printableObject; \n";
        std::cout << printableObject;
        std::cout << "\n";
    }

    struct HasPrint
    {
        void print() const
        {
            std::cout << "yahoo!!!";
        }
    };

    void test()
    {
        print(HasPrint{});
        print(123);
    }

}


void test_sfinae()
{
    type_traits_test::test();
    sfinae_test::test();
    enable_if_test::test();
    enable_if_type_traits_test::test();
    print_test::test();
}
