
#include "../services/service_http.hpp"
#include "../services/service_logger.hpp"
#include "../service_registry/service_registry.hpp"
#include "../catch/catch.hpp"
#include "../di/di.hpp"


namespace services { namespace ut {

    TEST_CASE("Can use Http Service", "[service_http]")
    {
        // ACT
        const auto spServiceHttp = ServiceRegistry::getInstance().getService<IServiceHttp>();

        // ASSERT
        REQUIRE(spServiceHttp != nullptr);
        REQUIRE(spServiceHttp->getName() == "Http Service");
        REQUIRE(spServiceHttp->get("http://www.worldcompany.com") == "http response");
    }

    TEST_CASE("Can use Logger Service", "[service_logger]")
    {
        // ACT
        const auto spServiceLogger = ServiceRegistry::getInstance().getService<IServiceLogger>();

        // ASSERT
        REQUIRE(spServiceLogger != nullptr);
        REQUIRE(spServiceLogger->getName() == "Logger Service");
        spServiceLogger->logInfo("help");
    }

    namespace {
        class ServiceHttpMock : public IServiceHttp
        {
        public:

            virtual std::string getName() const override
            {
                return std::string("Mock");
            }

            virtual std::string get(const std::string& url) override
            {
                return std::string();
            }

            virtual std::string post(const std::string& url) override
            {
                return std::string();
            }
        };
        struct SomeUserCode
        {
            SomeUserCode(
                const IServiceHttp& serviceHttp,
                const std::string& message)
                : m_serviceHttp(serviceHttp)
                , m_message(message)
            {
            }

            const IServiceHttp& m_serviceHttp;
            std::string m_message;
        };
    }

    TEST_CASE("Can inject Services", "[di]")
    {
        // ARRANGE
        namespace di = boost::di;
        auto injector = di::make_injector(
            di::bind<IServiceHttp>().to<ServiceHttpMock>(),
            di::bind<std::string>().to(std::string("hello"))
            );

        // ACT
        auto someUserCode = injector.create<SomeUserCode>();

        // ASSERT
        REQUIRE(someUserCode.m_serviceHttp.getName() == "Mock");
        REQUIRE(someUserCode.m_message == "hello");
    }

} } // namespace services::ut
