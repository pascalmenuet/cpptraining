
#pragma once


#include <string>


namespace services {

    class IService
    {
    public:

        virtual ~IService() = default;

        // Methods common to every services
        // ...

        virtual std::string getName() const = 0;
    };

} // namespace services
