
#include "service_registry.hpp"


namespace services {

    bool ServiceRegistry::registerService(const std::type_info& serviceInterfaceType, const ServiceFactory& serviceFactory)
    {
        auto& serviceInfo = m_services[std::type_index(serviceInterfaceType)];
        serviceInfo.first.reset();
        serviceInfo.second = serviceFactory;
        return true;
    }

    std::shared_ptr<IService> ServiceRegistry::getService(const std::type_info& serviceInterfaceType)
    {
        const auto iterServiceInfo = m_services.find(std::type_index(serviceInterfaceType));
        if (iterServiceInfo == end(m_services))
            return std::shared_ptr<IService>();
        auto& serviceInfo = iterServiceInfo->second;
        auto& spService = serviceInfo.first;
        if (!spService)
        {
            const auto& serviceFactory = serviceInfo.second;
            spService = serviceFactory();
        }
        return spService;
    }

    ServiceRegistry& ServiceRegistry::getInstance()
    {
        static ServiceRegistry s_serviceRegistry;
        return s_serviceRegistry;
    }

} // namespace services
