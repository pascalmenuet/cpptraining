
#pragma once


#include "service.hpp"
#include <typeinfo> // for std::type_info
#include <typeindex> // for std::type_index
#include <functional> // for std::function
#include <memory> // for std::unique_ptr and std::shared_ptr
#include <cassert> // for assert
#include <type_traits> // for std::is_base_of
#include <utility> // for std::pair
#include <map> // for std::map


namespace services { namespace ut {
    class ServiceRegistry_Friend;
 } }

namespace services {

    template< typename ServiceInterfaceT, typename ServiceImplementationT >
    class ServiceRegistrar;

    class ServiceRegistry
    {
        template< typename ServiceInterfaceT, typename ServiceImplementationT >
        friend class ServiceRegistrar;

        friend class ut::ServiceRegistry_Friend; // for accessing internals from unit tests only

    public:

        using ServiceFactory = std::function<std::unique_ptr<IService>()>;

        ServiceRegistry(const ServiceRegistry&) = delete;
        ServiceRegistry& operator=(const ServiceRegistry&) = delete;
        ServiceRegistry(ServiceRegistry&&) = delete;
        ServiceRegistry& operator=(ServiceRegistry&&) = delete;

        template< typename ServiceInterfaceT >
        std::shared_ptr<ServiceInterfaceT> getService()
        {
            static_assert(
                std::is_base_of<IService, ServiceInterfaceT>::value && !std::is_same<IService, ServiceInterfaceT>::value,
                "The requested service interface must derive from IService !!!"
                );
            const auto spService = getService(typeid(ServiceInterfaceT));
            if (!spService)
                return std::shared_ptr<ServiceInterfaceT>();
            assert(std::dynamic_pointer_cast<ServiceInterfaceT>(spService));
            return std::static_pointer_cast<ServiceInterfaceT>(spService);
        }

        static ServiceRegistry& getInstance();

    private:

        using ServiceInfo = std::pair<std::shared_ptr<IService>, ServiceFactory>;
        using MapServices = std::map<std::type_index, ServiceInfo>;

        ServiceRegistry() = default;

        bool registerService(const std::type_info& serviceInterfaceType, const ServiceFactory& serviceFactory);

        std::shared_ptr<IService> getService(const std::type_info& serviceInterfaceType);

        MapServices m_services;
    };

    template< typename ServiceInterfaceT, typename ServiceImplementationT >
    class ServiceRegistrar
    {
    public:

        ServiceRegistrar()
        {
            static_assert(
                std::is_base_of<IService, ServiceInterfaceT>::value && !std::is_same<IService, ServiceInterfaceT>::value,
                "You service interface must derive from IService !!!"
                );
            static_assert(
                std::is_base_of<ServiceInterfaceT, ServiceImplementationT>::value && !std::is_same<ServiceInterfaceT, ServiceImplementationT>::value,
                "You service implementation must derive from your service interface !!!"
                );
            m_isServiceRegistered = ServiceRegistry::getInstance().registerService(typeid(ServiceInterfaceT), &ServiceRegistrar::createService);
        }

        ServiceRegistrar(const ServiceRegistrar&) = delete;
        ServiceRegistrar& operator=(const ServiceRegistrar&) = delete;
        ServiceRegistrar(ServiceRegistrar&&) = delete;
        ServiceRegistrar& operator=(ServiceRegistrar&&) = delete;

        bool isServiceRegistered() const
        {
            return m_isServiceRegistered;
        }

    private:

        bool m_isServiceRegistered = false;

        static std::unique_ptr<IService> createService()
        {
            auto spServiceImplementation = std::make_unique<ServiceImplementationT>();
            return std::unique_ptr<IService>(std::move(spServiceImplementation));
        }
    };

} // namespace services
