
#pragma once


#include "../service_registry/service_registry.hpp"


namespace services { namespace ut {

    class ServiceRegistry_Friend
    {
    public:

        static void clearServices()
        {
            ServiceRegistry::getInstance().m_services.clear();
        }
    };

} } // namespace services::ut
