cmake_minimum_required (VERSION 2.8 FATAL_ERROR)
project (service_registry_test)

add_executable(
	service_registry_test
	service_registry_test.cpp
	service_registry_friend.hpp
	main.cpp
	)

target_link_libraries(service_registry_test service_registry)
