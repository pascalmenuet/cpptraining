
#include "../service_registry/service_registry.hpp"
#include "service_registry_friend.hpp"
#include "../catch/catch.hpp"


namespace services { namespace ut {

    namespace {

        struct IServiceDummy : public IService
        {
            virtual std::string test() = 0;
        };

        struct ServiceDummyImpl1 : public IServiceDummy
        {
            virtual std::string getName() const override { return "Dummy1"; }
            virtual std::string test() override { return "OK1"; }
        };

        struct ServiceDummyImpl2 : public IServiceDummy
        {
            virtual std::string getName() const override { return "Dummy2"; }
            virtual std::string test() override { return "OK2"; }
        };

        struct IServiceFake : public IService
        {
            virtual std::string fake() const = 0;
        };

        struct ServiceFakeImpl1 : public IServiceFake
        {
            virtual std::string getName() const override { return "Fake"; }
            virtual std::string fake() const override { return "OK"; }
        };

    }

    TEST_CASE("Can register/access services through the Service Registry", "[service_registry]")
    {
        SECTION("Register a service in the Service Registry")
        {
            // ARRANGE
            ServiceRegistry_Friend::clearServices();

            // ACT
            ServiceRegistrar<IServiceDummy, ServiceDummyImpl1> serviceRegistrar;

            // ASSERT
            REQUIRE(serviceRegistrar.isServiceRegistered());
        }

        SECTION("Register several services with the same interface in the Service Registry")
        {
            // ARRANGE
            ServiceRegistry_Friend::clearServices();

            // ACT
            ServiceRegistrar<IServiceDummy, ServiceDummyImpl1> serviceRegistrar1;

            // ASSERT
            REQUIRE(serviceRegistrar1.isServiceRegistered());
            const auto spService1 = ServiceRegistry::getInstance().getService<IServiceDummy>();
            REQUIRE(spService1 != nullptr);
            REQUIRE(spService1->getName() == "Dummy1");
            REQUIRE(spService1->test() == "OK1");

            // ACT
            ServiceRegistrar<IServiceDummy, ServiceDummyImpl2> serviceRegistrar2;

            // ASSERT
            REQUIRE(serviceRegistrar2.isServiceRegistered());
            const auto spService2 = ServiceRegistry::getInstance().getService<IServiceDummy>();
            REQUIRE(spService2 != nullptr);
            REQUIRE(spService2->getName() == "Dummy2");
            REQUIRE(spService2->test() == "OK2");
        }

        SECTION("Register several services with different interfaces in the Service Registry")
        {
            // ARRANGE
            ServiceRegistry_Friend::clearServices();

            // ACT
            ServiceRegistrar<IServiceDummy, ServiceDummyImpl1> serviceRegistrar1;
            ServiceRegistrar<IServiceFake, ServiceFakeImpl1> serviceRegistrar2;

            // ASSERT
            REQUIRE(serviceRegistrar1.isServiceRegistered());
            const auto spService1 = ServiceRegistry::getInstance().getService<IServiceDummy>();
            REQUIRE(spService1 != nullptr);
            REQUIRE(spService1->getName() == "Dummy1");
            REQUIRE(spService1->test() == "OK1");

            REQUIRE(serviceRegistrar2.isServiceRegistered());
            const auto spService2 = ServiceRegistry::getInstance().getService<IServiceFake>();
            REQUIRE(spService2 != nullptr);
            REQUIRE(spService2->getName() == "Fake");
            REQUIRE(spService2->fake() == "OK");
        }

        SECTION("Get an unregistered service in the Service Registry")
        {
            // ARRANGE
            ServiceRegistry_Friend::clearServices();

            // ACT
            const auto spService = ServiceRegistry::getInstance().getService<IServiceDummy>();

            // ASSERT
            REQUIRE(spService == nullptr);
        }

        SECTION("Get an registered service in the Service Registry")
        {
            // ARRANGE
            ServiceRegistry_Friend::clearServices();
            ServiceRegistrar<IServiceDummy, ServiceDummyImpl1> serviceRegistrar;

            // ACT
            const auto spService = ServiceRegistry::getInstance().getService<IServiceDummy>();

            // ASSERT
            REQUIRE(spService != nullptr);
            REQUIRE(spService->getName() == "Dummy1");
            REQUIRE(spService->test() == "OK1");
        }
    }

} } // namespace services::ut
