
#include "service_logger.hpp"
#include "../service_registry/service_registry.hpp"


namespace services { namespace {

    class ServiceLoggerImpl : public IServiceLogger
    {
    public:

        virtual std::string getName() const override
        {
            return std::string("Logger Service");
        }

        virtual void logInfo(const std::string& text) override
        {
            // ...
        }

        virtual void logError(const std::string& text) override
        {
            // ...
        }
    };

    ServiceRegistrar<IServiceLogger, ServiceLoggerImpl> registrarForHttpService;

} } // namespace services::'anonymous'
