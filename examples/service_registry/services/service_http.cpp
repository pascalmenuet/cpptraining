
#include "service_http.hpp"
#include "../service_registry/service_registry.hpp"


namespace services { namespace {

    class ServiceHttpImpl : public IServiceHttp
    {
    public:

        virtual std::string getName() const override
        {
            return std::string("Http Service");
        }

        virtual std::string get(const std::string& url) override
        {
            // ...
            return "http response";
        }

        virtual std::string post(const std::string& url) override
        {
            // ...
            return "http response";
        }
    };

    ServiceRegistrar<IServiceHttp, ServiceHttpImpl> registrarForHttpService;

} } // namespace services::'anonymous'
