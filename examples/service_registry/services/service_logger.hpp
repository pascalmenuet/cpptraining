
#pragma once


#include "../service_registry/service.hpp"


namespace services {

    class IServiceLogger : public IService
    {
    public:

        // Methods specific to the Logger Service
        // ...

        virtual void logInfo(const std::string& text) = 0;

        virtual void logError(const std::string& text) = 0;
    };

} // namespace services
