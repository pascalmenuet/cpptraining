
#pragma once


#include "../service_registry/service.hpp"


namespace services {

    class IServiceHttp : public IService
    {
    public:

        // Methods specific to the Http Service
        // ...

        virtual std::string get(const std::string& url) = 0;

        virtual std::string post(const std::string& url) = 0;
    };

} // namespace services
