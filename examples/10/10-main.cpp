
void test_sequence_containers();
void test_associative_containers();
void test_algorithms();

int main()
{
    test_sequence_containers();
    test_associative_containers();
    test_algorithms();
    return 0;
}
