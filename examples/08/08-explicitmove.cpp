#include <cassert>
#include <string>

struct K
{
    K(std::string&& s)
        : m_s{ std::move(s) }
    {
    }
    std::string m_s;
};

void test_explicitmove()
{
    std::string s("move!");
    K k(std::move(s));
    assert(k.m_s == "move!");
    assert(s.empty());
}
