
int F()
{
    return 42;
}

int i{};

void test_ref()
{
    int& lv_ref1 = i;

    // Compilation Erro
    // int& lv_ref2 = F();

    const int& c_lv_ref1 = i;

    // OK and extend the lifetime of the returned object
    const int& c_lv_ref2 = F();

    // Compilation Erro
    // int&& rv_ref1 = i;

    // OK and extend the lifetime of the unnamed object
    int&& rv_ref2 = int{ 3 };

    // Compilation Erro
    // const int&& c_rv_ref1 = i;

    // OK and extend the lifetime of the returned object
    const int&& c_rv_ref2 = F();
}
