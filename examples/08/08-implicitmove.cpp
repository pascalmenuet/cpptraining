#include <cassert>
#include <vector>
#include <numeric>

std::vector<int> createVec() {
    std::vector<int> vec(10);
    std::iota(vec.begin(), vec.end(), 1);
    return vec;
}

struct K {
    K(std::string s) : m_s{ s }
    {
    }
    std::string m_s;
};

void testmove()
{
    auto vec = createVec();
    assert((vec == std::vector<int>{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}));

    K k(std::string("move!"));
    assert(k.m_s == "move!");
}
