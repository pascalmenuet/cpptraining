
#include <array>

int n1 = 12345; // Classic C syntax
int n2(-321); // C++ constructor-like syntax
int n3 = int(+321); // C++ copy initialization syntax
int n4 = { 789 }; // C++11 aggregate/list initialization syntax
std::array<int, 4> n5 = { 5, 4, 3, 2 }; // C++11 aggregate/list initialization syntax
int n6{ 654 }; // C++11 direct initialization syntax
int n7 = int{ 1 }; // C++11 copy initialization syntax
int n8{}; // C++11 value initialization syntax
int n9(); // Most vexing parse: declaration of a function !!!
