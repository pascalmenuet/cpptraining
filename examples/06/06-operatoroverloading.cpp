
#include <string>
#include <cassert>

class Over
{
public:

    Over(const std::string& s)
        : m_s(s)
    {
    }

    Over& operator++()
    {
        if (!m_s.empty())
            ++m_s[m_s.length() - 1];
        return *this;
    }

    Over operator++(int)
    {
        Over previousValue = *this;
        if (!m_s.empty())
            ++m_s[m_s.length() - 1];
        return previousValue;
    }

    friend bool operator==(const Over& o1, const Over& o2);

    friend bool operator<(const Over& o1, const Over& o2);

private:

    std::string m_s;
};

bool operator==(const Over& o1, const Over& o2)
{
    return o1.m_s == o2.m_s;
}

bool operator<(const Over& o1, const Over& o2)
{
    return o1.m_s < o2.m_s;
}

void testoperatoroverloading()
{
    Over over1("abc");
    Over over2("abc");
    assert(over1 == over2);

    auto over3 = over2++;
    assert(over1 < over2);
    assert(over1 == over3);

    auto over4 = ++over3;
    assert(over1 < over4);
    assert(over1 < over3);
    assert(over3 == over4);
}
