#include <string>
#include <cassert>

struct K {
    K(int i)
        : m_i(i)
    {}

    explicit K(double d)
        : m_i(static_cast<int>(d))
    {}

    operator char() const
    {
        return static_cast<char>(m_i);
    }

    operator bool() const
    {
        return true;
    }

    explicit operator std::string() const
    {
        return std::to_string(m_i);
    }

    int m_i;
};

static int getIntVal(const K& k)
{
    return k.m_i;
}

void f(bool b)
{
}

void func(K k)
{
}

void testcast()
{
    const K k(123);

    func(K(12.3));

    f(k);

    if (!k)
        return;

    const bool b = static_cast<bool>(k);

    const char c = k;
    assert(c == 123);

    const std::string s = static_cast<std::string>(k);
    assert(s == "123");

    const auto i = getIntVal(K(3.1415));
    assert(i == 3);
}
