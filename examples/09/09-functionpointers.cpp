
#include <cassert>

int g_number;

int squarePlusNumber(int i)
{
    return i * i + g_number;
}

using Func = int(int);
using FuncPtr = Func*;

void test_functionpointers()
{
    g_number = 12;

    // Store address of squarePlusNumber for later use
    FuncPtr p = &squarePlusNumber;

    // ...

    // call squarePlusNumber using the function pointer
    const auto result = (*p)(10);

    assert(result == 112);
}
