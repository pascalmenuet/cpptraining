#include <vector>
#include <algorithm>
#include <iostream>

void test_lambdas() {
    std::vector<int> v = { 1, 2, 3, 4 };

    std::transform(v.begin(), v.end(), v.begin(), [](int elem) {
        return elem * elem;
    });

    auto finder = [](const auto& elem) {
        return elem == 9;
    };
    auto iter = std::find_if(v.begin(), v.end(), finder);
    if (iter != v.end())
        std::cout << "Found 9!!!\n";

    int sum = 0;
    std::for_each(v.begin(), v.end(), [&sum](const auto& elem) {
        sum += elem;
    });
    std::cout << "Sum of squares = " << sum << "\n";
}
