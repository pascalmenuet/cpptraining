
void test_functionpointers();
void test_functionobjects();
void use();
void test_lambdas();
void test_stdfunction();

int main()
{
    test_functionpointers();
    test_functionobjects();
    use();
    test_lambdas();
    test_stdfunction();
    return 0;
}
