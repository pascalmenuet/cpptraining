#include <functional>
#include <iostream>
#include <map>
#include <string>

class Menu
{
public:

    using CommandSignature = void(const std::string&);
    using Command = std::function<CommandSignature>;

    void addCommand(const std::string& commandName, Command command)
    {
        m_commands[commandName] = command;
    }

    void showMenuThenExecuteCommand() const
    {
        std::cout << "List of available commands:\n";
        for (const auto& command : m_commands)
            std::cout << command.first << "\n";
        std::cout << "Which command do you choose ? ";
        std::string commandName;
        std::cin >> commandName;
        const auto commandIterator = m_commands.find(commandName);
        if (commandIterator == m_commands.end())
            std::cout << "Unknown command\n";
        else
        {
            const auto& command = commandIterator->second;
            command(commandIterator->first);
        }
    }

private:

    std::map<std::string, Command> m_commands;
};

struct Save
{
    void operator()(const std::string& commandName)
    {
        std::cout << "Saving...\n";
    }
};

void close(const std::string& commandName)
{
    std::cout << "Closing...\n";
}

void test_stdfunction() {
    Menu menu;

    const auto open = [](const std::string& commandName)
    {
        std::cout << "Opening...\n";
    };
    menu.addCommand("Open", open);

    Save save;
    menu.addCommand("Save", save);

    menu.addCommand("Close", close);

    menu.showMenuThenExecuteCommand();
}
