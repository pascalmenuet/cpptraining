#include <cassert>

class SquarePlusNumber
{
public:

    SquarePlusNumber(int number)
        : m_number(number)
    {}

    int operator()(int i) const {
        return i * i + m_number;
    }

private:
    int m_number;
};

void test_functionobjects() {
    // construct the function object
    SquarePlusNumber squarePlusNumber(12);

    // call the function object
    auto result = squarePlusNumber(10); 
    assert(result == 112);

    // construct-then-call-then-destruct the function object
    result = SquarePlusNumber(5)(9);
    assert(result == 86);
}
