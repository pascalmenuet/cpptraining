#include <string>
#include <cassert>
#include <cstdlib>

class FileBase
{
public:

    FileBase(int t) {}
};

class Feature
{
public:
    Feature(int i = 0) : m_i{} {}
    int m_i;
};

class File : public FileBase, private Feature
{
public:

    File(int i)
        : Feature(i)
        , FileBase(i)
        , m_f{}
        , m_s{"sdfsdf"}
    {
        m_f = 5;
    }

    File(const std::string& fileName)
        : FileBase(456)
    {
        auto pFile = fopen(fileName.c_str(), "r");
    }

    File()
        : FileBase(456)
    {
    }

    ~File()
    {
        // ...
    } // ~m_f, ~m_i, ~Feature, ~FileBase

    File(const File&) = delete;
    File& operator=(const File&) = delete;

    const std::string& get() const
    {
        return m_s;
    }

    void set(const std::string& s)
    {
        m_s = s;
    }

private:

    std::string m_s{"he"};
    Feature m_f{ 123 };
};

void f(const File& f)
{
    f.get();
}


class C
{
public:

    const std::string& get() const
    {
        return m_s;
    }

    void set(const std::string& s)
    {
        m_s = s;
    }

private:

    std::string m_s;
};

void testcopyconstruct()
{
    C c;
    c.set("warum weil");

    C c2(c); // Copy construction
    assert(c2.get() == "warum weil");

    C c3 = c; // Copy construction too
    assert(c3.get() == "warum weil");

    C c4;
    C* pC = &c4;
    pC = nullptr;
    c4 = c;
    (*pC).operator=(c);

    C c5 = c;
}
