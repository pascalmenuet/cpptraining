#include <string>
#include <cassert>

class Interface
{
public:
    virtual ~Interface() = default;

    virtual std::string F1() = 0;

    virtual std::string F2() = 0;
};

class Implem : public Interface
{
public:
    virtual std::string F1() override
    { return "Implem::F1()"; }

    virtual std::string F2() override
    { return "Implem::F2()"; }
};

void testinterface()
{
    Implem* pImplem = new Implem;
    assert(pImplem->F1() == "Implem::F1()");
    assert(pImplem->F2() == "Implem::F2()");

    Interface& interface = *pImplem;
    assert(interface.F1() == "Implem::F1()");
    assert(interface.F2() == "Implem::F2()");

    delete pImplem;

    // Compiler error:
    // abstract class cannot be instantiated
    // Interface interfaceObject{};
}
