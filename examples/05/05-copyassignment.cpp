#include <string>
#include <cassert>

class C
{
public:
    const std::string& get() const
    {
        return m_s;
    }

    void set(const std::string& s)
    {
        m_s = s;
    }

    // copy assignment operator
    C& operator=(const C& other)
    {
        m_s = other.m_s;
        return *this;
    }

private:
    std::string m_s;
};

void testcopyassignment()
{
    C c;
    c.set("first");

    C c2;

    // Call the copy assignment operator
    c2 = c;
    assert(c2.get() == "first" && c.get() == "first");

    c2.set("next");
    assert(c2.get() == "next" && c.get() == "first");
}
