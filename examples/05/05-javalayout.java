
class A
{
    public double m_d = 3.14;
    public int m_i = 42;
};

class B
{
    public float m_f = 2.5f;
    public A m_a = null;
};

class Test
{
    static void test(string[] args)
    {
        A a = new A();
        B b = new B();
        b.m_a = a;
        double d = 1234.5678;
        a.m_d = d;
    }
}
