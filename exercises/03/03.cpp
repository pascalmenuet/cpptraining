
// Fix compilation issues
// Question: what is the value of val at the end of main ?

void F1(long& val)
{
    ++val;
}

void F2(long val)
{
    ++val;
}

void F3(long* val)
{
    ++val;
}

void F4(long* val)
{
    val = 8;
}

long F5(const long& val)
{
    ++val;
    return val + 1;
}

long F5(const long* val)
{
    ++val;
    return *val + 1;
}

void main()
{
    int val = 12;
    F1(val);
    F2(val);
    F3(val);
    F4(val);
    val = F5(val);
    val = F6(val);
}
