
#include <string>
#include <array>
#include <vector>
#include <thread>

std::size_t someFunction(std::string someParam) {
    double someArithmeticObject;
    int otherArithmeticObject = 1;
    std::string someStringObject;
    static std::vector<int> someVector = { 1, 2, 3 };
    return someParam.size();
}

int incCounter();

namespace foo {
    std::array<int, 5> someArray;
}

int c = incCounter();

extern std::array<int, 10> someOtherArray;

namespace foo {
    namespace boom {
        class C;
    }
}

thread_local int yahooo;

int incCounter()
{
    using namespace foo;
    static int i;
    i = someOtherArray[i];
    return ++i;
}

foo::boom::C* p = nullptr;

namespace foo {
    namespace boom {
        class C
        {
            char member;
            static std::string otherMember;
        };
    }
}

using foo::boom::C;

C* p2 = new C;

std::array<int, 10> someOtherArray;

int main() {}
