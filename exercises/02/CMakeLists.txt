cmake_minimum_required(VERSION 3.3)
project(exercise02)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14")

set(SOURCE_FILES
        02.cpp)

add_executable(exercise02 ${SOURCE_FILES})