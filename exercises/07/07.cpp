
#include <cstdio>
#include <cstdint>
#include <cassert>
#include <vector>
#include <memory>

namespace exercise1 {

    class File
    {
    public:

        File() = default;

        File(const char* fileName, const char* mode)
            : m_pCFile{ std::fopen(fileName, mode) }
        {
        }

        File(const File&) = delete;

        ~File()
        {
            close();
        }

        File& operator=(const File&) = delete;

        explicit operator bool() const
        {
            return m_pCFile != nullptr;
        }

        void close()
        {
            if (!m_pCFile)
                return;
            std::fclose(m_pCFile);
            m_pCFile = nullptr;
        }

        void open(const char* fileName, const char* mode)
        {
            close();
            m_pCFile = std::fopen(fileName, mode);
        }

        void read(std::vector<std::uint8_t>& buffer) const
        {
            if (!m_pCFile)
                return;
            std::fread(buffer.data(), sizeof(std::uint8_t), buffer.size(), m_pCFile);
        }

        void write(const std::vector<std::uint8_t>& buffer)
        {
            if (!m_pCFile)
                return;
            std::fwrite(buffer.data(), sizeof(std::uint8_t), buffer.size(), m_pCFile);
        }

    private:

        std::FILE* m_pCFile = nullptr;
    };

    void test()
    {
        {
            File file("test.txt", "w");
            if (!file)
                return;
            std::vector<std::uint8_t> buffer{ 90,80,70,60,50,40,30,20,10 };
            file.write(buffer);
        }
        {
            File file("test.txt", "r");
            if (!file)
                return;
            std::vector<std::uint8_t> buffer(9);
            file.read(buffer);
            assert((buffer == std::vector<std::uint8_t>{ 90,80,70,60,50,40,30,20,10 }));
        }
    }

}

namespace exercise2 {

    class File
    {
    public:

        File() = default;

        File(const char* fileName, const char* mode)
            : m_upCFile{ std::fopen(fileName, mode) }
        {
        }

        explicit operator bool() const
        {
            return m_upCFile != nullptr;
        }

        void close()
        {
            if (!m_upCFile)
                return;
            std::fclose(m_upCFile.get());
            m_upCFile.reset();
        }

        void open(const char* fileName, const char* mode)
        {
            close();
            m_upCFile.reset(std::fopen(fileName, mode));
        }

        void read(std::vector<std::uint8_t>& buffer) const
        {
            if (!m_upCFile)
                return;
            std::fread(buffer.data(), sizeof(std::uint8_t), buffer.size(), m_upCFile.get());
        }

        void write(const std::vector<std::uint8_t>& buffer)
        {
            if (!m_upCFile)
                return;
            std::fwrite(buffer.data(), sizeof(std::uint8_t), buffer.size(), m_upCFile.get());
        }

    private:

        struct FileCloser
        {
            void operator()(FILE* pCFile) const
            {
                ::std::fclose(pCFile);
            }
        };

        std::unique_ptr<std::FILE, FileCloser> m_upCFile;
    };

    void test()
    {
        {
            File file("test.txt", "w");
            if (!file)
                return;
            std::vector<std::uint8_t> buffer{ 90,80,70,60,50,40,30,20,10 };
            file.write(buffer);
        }
        {
            File file("test.txt", "r");
            if (!file)
                return;
            std::vector<std::uint8_t> buffer(9);
            file.read(buffer);
            assert((buffer == std::vector<std::uint8_t>{ 90, 80, 70, 60, 50, 40, 30, 20, 10 }));
        }
    }

}

int main()
{
    exercise1::test();
    exercise2::test();
    return 0;
}
