
#include "12-tasklauncher.h"

class Square : public Shape
{
    virtual void draw() const override
    {
        std::cout << "Drawing Square\n";
    }
};

class Triangle : public Shape
{
    virtual void draw() const override
    {
        std::cout << "Drawing Triangle\n";
    }
};

class Circle : public Shape
{
    virtual void draw() const override
    {
        std::cout << "Drawing Circle\n";
    }
};

TaskLauncher::TaskLauncher()
    : m_thread([this]()
    {
        while (!m_threadShouldQuit)
        {
            std::this_thread::sleep_for(std::chrono::seconds(1));
            TaskQueue::Task task;
            if (this->m_tasksQueue.peekTask(task))
                task();
        }
    })
{
}

TaskLauncher::~TaskLauncher()
{
    m_tasksQueue.pushTask([this]()
    {
        this->m_threadShouldQuit = true;
    });
    m_thread.join();
}

void TaskLauncher::addCircle()
{
    m_tasksQueue.pushTask([this]()
    {
        std::cout << "New circle created\n";
        this->m_shapes.push_back(std::make_unique<Circle>());
    });
}

void TaskLauncher::addSquare()
{
    m_tasksQueue.pushTask([this]()
    {
        std::cout << "New square created\n";
        this->m_shapes.push_back(std::make_unique<Square>());
    });
}

void TaskLauncher::addTriangle()
{
    m_tasksQueue.pushTask([this]()
    {
        std::cout << "New triangle created\n";
        this->m_shapes.push_back(std::make_unique<Triangle>());
    });
}

void TaskLauncher::drawShapes()
{
    m_tasksQueue.pushTask([this]()
    {
        std::cout << "List of shapes:\n";
        for (const auto& upShape : this->m_shapes)
            upShape->draw();
    });
}
