
#pragma once


#include <functional>
#include <mutex>
#include <deque>


class TaskQueue
{
public:

    using Task = std::function<void()>;

    void pushTask(Task task);

    bool peekTask(Task& task);

private:

    std::mutex m_mutex;
    std::deque<Task> m_tasks;
};
