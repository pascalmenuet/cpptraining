#pragma once


#include "12-taskqueue.h"
#include <string>
#include <iostream>
#include <thread>
#include <vector>
#include <memory>


class Shape
{
public:

    virtual ~Shape() = default;

    virtual void draw() const = 0;
};


class TaskLauncher
{
public:

    TaskLauncher();

    ~TaskLauncher();

    void menuLoop()
    {
        std::string command;
        do
        {
            std::cout << "Choose a command\n";
            std::cout << "Type 'c' + ENTER for creating a circle\n";
            std::cout << "Type 's' + ENTER for creating a square\n";
            std::cout << "Type 't' + ENTER for creating a triangle\n";
            std::cout << "Type 'd' + ENTER for displaying the list of shapes\n";
            std::cout << "Type 'q' + ENTER for leaving\n";
            std::cout << "Choice ? ";
            std::cin >> command;

            if (command == "c")
                addCircle();
            else if (command == "s")
                addSquare();
            else if (command == "t")
                addTriangle();
            else if (command == "d")
                drawShapes();

        } while (command != "q");
    }

private:

    void addCircle();
    void addSquare();
    void addTriangle();
    void drawShapes();

    bool m_threadShouldQuit = false;
    TaskQueue m_tasksQueue;
    std::thread m_thread;
    std::vector<std::unique_ptr<Shape>> m_shapes; // only accessed from the thread
};

inline void test_tasks()
{
    TaskLauncher taskLauncher;
    taskLauncher.menuLoop();
}
