
#include "12-taskqueue.h"

void TaskQueue::pushTask(Task task)
{
    std::lock_guard<std::mutex> lock(m_mutex);

    m_tasks.push_back(std::move(task));
}

bool TaskQueue::peekTask(Task& task)
{
    std::lock_guard<std::mutex> lock(m_mutex);

    if (m_tasks.empty())
        return false;
    task = std::move(m_tasks.front());
    m_tasks.pop_front();
    return true;
}
