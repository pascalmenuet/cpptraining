
#include "11-filesystem/api.h"
#include <cassert>
#include <vector>
#include <algorithm>


template< typename SystemT>
void test_filesystem_version()
{
    using EntryType = typename SystemT::EntryType;
    using AccessRights = typename SystemT::AccessRights;
    using FileFacade = typename SystemT::FileFacade;
    using FolderFacade = typename SystemT::FolderFacade;

    {
        SystemT system;

        auto folder = system.createFolder("/folder", AccessRights::All);
        assert(folder);
        assert(folder.getType() == EntryType::Folder);
        assert(folder.getParent());
        assert(!folder.getParent().getParent());
        assert(folder.getPath() == "/folder");
        assert(folder.getAccessRights() == AccessRights::All);
        assert(folder.getChildrenCount() == 0);
    }

    {
        SystemT system;

        auto file = system.createFile("/file", AccessRights::Read, "contents");
        assert(file);
        assert(file.getType() == EntryType::File);
        assert(file.getParent());
        assert(!file.getParent().getParent());
        assert(file.getPath() == "/file");
        assert(file.getAccessRights() == AccessRights::Read);
        assert(file.getContents() == "contents");
    }

    {
        SystemT system;

        auto entry = system.findEntry("/cheese");
        assert(!entry);
    }

    {
        SystemT system;

        auto result = system.deleteEntry("/cake");
        assert(!result);
    }

    {
        SystemT system;

        assert(system.createFolder("/pim", AccessRights::All));
        assert(system.createFolder("/pim/pam", AccessRights::Write));
        assert(system.createFolder("/pim/pam/poom", AccessRights::Read));
        assert(system.createFile("/pim/pam/paf", AccessRights::All, "something"));
        assert(system.createFile("/pouf", AccessRights::Read, "nothing"));
        assert(system.createFolder("/boom", AccessRights::Write));

        auto entry = system.findEntry("");
        assert(entry);
        assert(entry.getType() == EntryType::Folder);
        assert(!SystemT::facade_cast_2<FileFacade>(entry));
        auto folder = SystemT::facade_cast_2<FolderFacade>(entry);
        assert(folder);
        assert(folder.getChildrenCount() == 3);
        std::vector<std::string> childrenPaths;
        folder.visitChildren([&childrenPaths](const auto& child) {
            childrenPaths.push_back(child.getPath());
        });
        std::sort(childrenPaths.begin(), childrenPaths.end());
        assert(childrenPaths.size() == 3);
        assert(childrenPaths[0] == "/boom");
        assert(childrenPaths[1] == "/pim");
        assert(childrenPaths[2] == "/pouf");

        entry = system.findEntry("/pim/paf");
        assert(!entry);

        entry = system.findEntry("/pim/pam");
        assert(!SystemT::facade_cast_2<FileFacade>(entry));
        folder = SystemT::facade_cast_2<FolderFacade>(entry);
        assert(folder);
        assert(folder.getChildrenCount() == 2);
        childrenPaths.clear();
        folder.visitChildren([&childrenPaths](const auto& child) {
            childrenPaths.push_back(child.getPath());
        });
        std::sort(childrenPaths.begin(), childrenPaths.end());
        assert(childrenPaths.size() == 2);
        assert(childrenPaths[0] == "/pim/pam/paf");
        assert(childrenPaths[1] == "/pim/pam/poom");

        entry = system.findEntry("/pim/pam/paf");
        assert(entry);
        assert(!SystemT::facade_cast_2<FolderFacade>(entry));
        auto file = SystemT::facade_cast_2<FileFacade>(entry);
        assert(file);
        assert(file.getContents() == "something");
        assert(file.getParent());
        assert(file.getParent().getPath() == "/pim/pam");

        assert(system.deleteEntry("/pim"));
        assert(!system.findEntry("/pim"));
        assert(!system.deleteEntry("/pim"));
        assert(system.deleteEntry("/pouf"));
        assert(!system.findEntry("/pouf"));
        assert(!system.deleteEntry("/pouf"));
    }
}

void test_filesystem()
{
    test_filesystem_version<filesystem::SystemFacade>();
    test_filesystem_version<filesystem::v1::SystemFacade>();
    test_filesystem_version<filesystem::v2::SystemFacade>();
}
