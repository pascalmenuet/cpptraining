
#include "api-v2.h"
#include "implem-v2.h"


namespace filesystem { namespace v2 {

    SystemFacade::SystemFacade()
        : m_upImpl{ std::make_unique<detail::System>() }
    {
    }

    SystemFacade::~SystemFacade() = default;

    FolderFacade SystemFacade::createFolder(const std::string& entryPath, AccessRights accessRights)
    {
        return FolderFacade{ m_upImpl->createFolder(entryPath, accessRights) };
    }

    FileFacade SystemFacade::createFile(const std::string& entryPath, AccessRights accessRights, const std::string& contents)
    {
        return FileFacade{ m_upImpl->createFile(entryPath, accessRights, contents) };
    }

    EntryFacade SystemFacade::findEntry(const std::string& entryPath) const
    {
        return EntryFacade{ m_upImpl->findEntry(entryPath) };
    }

    bool SystemFacade::deleteEntry(const std::string& entryPath)
    {
        return m_upImpl->deleteEntry(entryPath);
    }

    EntryType EntryFacade::getType() const
    {
        if (!m_pEntry)
            return EntryType::Unknown;
        return m_pEntry->getType();
    }

    FolderFacade EntryFacade::getParent() const
    {
        if (!m_pEntry)
            return FolderFacade{};
        return FolderFacade{ m_pEntry->getParent() };
    }

    std::string EntryFacade::getPath() const
    {
        if (!m_pEntry)
            return std::string{};
        return m_pEntry->getPath();
    }

    AccessRights EntryFacade::getAccessRights() const
    {
        if (!m_pEntry)
            return AccessRights{};
        return m_pEntry->getAccessRights();
    }

    unsigned int FolderFacade::getChildrenCount() const
    {
        if (!m_pEntry)
            return 0;
        const auto& children = m_pEntry->getChildren();
        return static_cast<unsigned int>(children.size());
    }

    void FolderFacade::visitChildren(EntryVisitor entryVisitor) const
    {
        if (!m_pEntry)
            return;
        auto& children = m_pEntry->getChildren();
        std::for_each(begin(children), end(children), [&entryVisitor](auto& child)
        {
            EntryFacade entryFacade(&child.second);
            entryVisitor(entryFacade);
        });
    }

    std::string FileFacade::getContents() const
    {
        if (!m_pEntry)
            return std::string{};
        return m_pEntry->getContents();
    }

    template<>
    FolderFacade facade_cast<FolderFacade>(const EntryFacade& entryFacade)
    {
        if (entryFacade.getType() != EntryType::Folder)
            return FolderFacade{};
        return FolderFacade{ entryFacade.m_pEntry };
    }

    template<>
    FileFacade facade_cast<FileFacade>(const EntryFacade& entryFacade)
    {
        if (entryFacade.getType() != EntryType::File)
            return FileFacade{};
        return FileFacade{ entryFacade.m_pEntry };
    }

} } // namespace filesystem::v2
