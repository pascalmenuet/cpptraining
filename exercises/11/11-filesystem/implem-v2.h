
#pragma once


#include "api.h"
#include <memory>
#include <map>
#include <algorithm>
#include <utility>
#include <eggs/variant.hpp>


namespace filesystem { namespace v2 { namespace detail {

    class Entry
    {
    public:

        Entry(
            Entry* pParent,
            const std::string& name,
            AccessRights accessRights
            )
            : m_pParent(pParent)
            , m_name(name)
            , m_accessRights(accessRights)
            , m_variant{ FolderData{} }
        {
        }

        Entry(
            Entry* pParent,
            const std::string& name,
            AccessRights accessRights,
            const std::string& contents
            )
            : m_pParent(pParent)
            , m_name(name)
            , m_accessRights(accessRights)
            , m_variant{ FileData{ contents } }
        {
        }

        EntryType getType() const
        {
            return m_variant.which() ? EntryType::Folder : EntryType::File;
        }

        Entry* getParent() const
        {
            return m_pParent;
        }

        const std::string& getName() const
        {
            return m_name;
        }

        std::string getPath() const
        {
            if (m_name.empty() || !m_pParent)
                return std::string{};
            std::string path = m_pParent->getPath();
            path += '/';
            path += m_name;
            return path;
        }

        AccessRights getAccessRights() const
        {
            return m_accessRights;
        }

        Entry* addChild(Entry&& child)
        {
            assert(getType() == EntryType::Folder);
            auto& children = eggs::variants::get<FolderData>(m_variant).m_children;
            const auto result = children.emplace(child.getName(), std::move(child));
            if (!result.second)
                return nullptr;
            return &result.first->second;
        }

        Entry* findChild(const std::string& childName)
        {
            assert(getType() == EntryType::Folder);
            auto& children = eggs::variants::get<FolderData>(m_variant).m_children;
            const auto childIterator = children.find(childName);
            if (childIterator == children.end())
                return nullptr;
            return &childIterator->second;
        }

        bool deleteChild(const std::string& childName)
        {
            assert(getType() == EntryType::Folder);
            auto& children = eggs::variants::get<FolderData>(m_variant).m_children;
            const auto childIterator = children.find(childName);
            if (childIterator == children.end())
                return false;
            children.erase(childIterator);
            return true;
        }

        const std::string& getContents() const
        {
            assert(getType() == EntryType::File);
            auto& contents = eggs::variants::get<FileData>(m_variant).m_contents;
            return contents;
        }

        using EntriesOwners = std::map<std::string, Entry>;

        const EntriesOwners& getChildren() const
        {
            assert(getType() == EntryType::Folder);
            auto& children = eggs::variants::get<FolderData>(m_variant).m_children;
            return children;
        }

        EntriesOwners& getChildren()
        {
            assert(getType() == EntryType::Folder);
            auto& children = eggs::variants::get<FolderData>(m_variant).m_children;
            return children;
        }

    private:

        struct FileData
        {
            std::string m_contents;
        };

        struct FolderData
        {
            EntriesOwners m_children;
        };

        Entry* m_pParent{};
        std::string m_name{};
        AccessRights m_accessRights{};
        eggs::variant<FileData, FolderData> m_variant{};
    };

    class System
    {
    public:

        Entry* createFolder(const std::string& entryPath, AccessRights accessRights)
        {
            const auto parentAndName = findParentAndExtractName(entryPath);
            if (parentAndName.first == nullptr || parentAndName.second.empty())
                return nullptr;
            Entry folder(parentAndName.first, parentAndName.second, accessRights);
            const auto pEntry = parentAndName.first->addChild(std::move(folder));
            return pEntry;
        }

        Entry* createFile(const std::string& entryPath, AccessRights accessRights, const std::string& contents)
        {
            const auto parentAndName = findParentAndExtractName(entryPath);
            if (parentAndName.first == nullptr || parentAndName.second.empty())
                return nullptr;
            Entry file(parentAndName.first, parentAndName.second, accessRights, contents);
            const auto pEntry = parentAndName.first->addChild(std::move(file));
            return pEntry;
        }

        Entry* findEntry(const std::string& entryPath) const
        {
            if (entryPath.empty())
                return m_upRoot.get();
            const auto parentAndName = findParentAndExtractName(entryPath);
            if (parentAndName.first == nullptr || parentAndName.second.empty())
                return false;
            return parentAndName.first->findChild(parentAndName.second);
        }

        bool deleteEntry(const std::string& entryPath)
        {
            const auto parentAndName = findParentAndExtractName(entryPath);
            if (parentAndName.first == nullptr || parentAndName.second.empty())
                return false;
            return parentAndName.first->deleteChild(parentAndName.second);
        }

    private:

        std::pair<Entry*, std::string> findParentAndExtractName(const std::string& entryPath) const
        {
            if (entryPath.empty() || entryPath[0] != '/')
                return std::make_pair(nullptr, std::string{});
            auto pParent = m_upRoot.get();
            std::string::size_type posNameBegin = 1;
            auto posNameEnd = entryPath.find('/', posNameBegin);
            while (posNameEnd != std::string::npos)
            {
                auto name = entryPath.substr(posNameBegin, posNameEnd - posNameBegin);
                pParent = pParent->findChild(name);
                if (!pParent || pParent->getType() != EntryType::Folder)
                    return std::make_pair(nullptr, std::string{});
                posNameBegin = posNameEnd + 1;
                posNameEnd = entryPath.find('/', posNameBegin);
            }
            auto name = entryPath.substr(posNameBegin);
            return std::make_pair(pParent, name);
        }

        std::unique_ptr<Entry> m_upRoot = std::make_unique<Entry>(nullptr, "", AccessRights::All);
    };

} } } // namespace filesystem::v2::detail
