
#include <memory>
#include <algorithm>
#include <cstring>
#include <cassert>

class String final
{
public:

    String() = default;

    String(String&&) = default;

    String(const String& s)
        : m_length{ s.m_length }
        , m_upChars{}
    {
        if (!m_length)
            return;
        m_upChars = std::make_unique<char []>(m_length + 1);
        std::copy_n(s.m_upChars.get(), m_length, m_upChars.get());
        m_upChars.get()[m_length] = 0;
    }

    String(const char* s)
        : m_length{ std::strlen(s) }
        , m_upChars{}
    {
        if (!m_length)
            return;
        m_upChars = std::make_unique<char []>(m_length + 1);
        std::copy_n(s, m_length, m_upChars.get());
        m_upChars.get()[m_length] = 0;
    }

    ~String() = default;

    String& operator=(String&& s) = default;

    String& operator=(const String& s)
    {
        String(s).swap(*this);
        return *this;
    }

    String& operator=(const char* s)
    {
        String(s).swap(*this);
        return *this;
    }

    void swap(String& s)
    {
        using std::swap;
        swap(m_upChars, s.m_upChars);
        swap(m_length, s.m_length);
    }

    const char* getChars() const
    {
        return m_upChars ? m_upChars.get() : "";
    }

    std::size_t getLength() const
    {
        return m_length;
    }

private:

    std::unique_ptr<char []> m_upChars{};
    std::size_t m_length{};
};

bool operator==(const String& s1, const String& s2)
{
    return s1.getLength() == s2.getLength() && std::strcmp(s1.getChars(), s2.getChars()) == 0;
}

bool operator!=(const String& s1, const String& s2)
{
    return !operator==(s1, s2);
}

bool operator<(const String& s1, const String& s2)
{
    if (s1.getLength() < s2.getLength())
        return true;
    if (s1.getLength() == s2.getLength())
        return std::strcmp(s1.getChars(), s2.getChars()) < 0;
    return false;
}

void test_string()
{
    {
        String s;

        assert(s.getChars() != nullptr);
        assert(s.getChars()[0] == 0);
        assert(s == "");
        assert(s.getLength() == 0);
    }

    {
        String s("hello");

        assert(s.getChars() != nullptr);
        assert(std::strcmp(s.getChars(), "hello") == 0);
        assert(s == "hello");
        assert(s.getLength() == 5);
    }

    {
        String s1("hello");
        String s2(s1);

        assert(s1.getChars() != nullptr);
        assert(std::strcmp(s1.getChars(), "hello") == 0);
        assert(s1 == "hello");
        assert(s1.getLength() == 5);

        assert(s2.getChars() != nullptr);
        assert(std::strcmp(s2.getChars(), "hello") == 0);
        assert(s2 == "hello");
        assert(s2.getLength() == 5);
    }

    {
        String s1("hello");
        String s2(std::move(s1));

        // Testing a moved-from object is tricky
        assert(s1.getChars() != nullptr);
        assert(s1.getChars()[0] == 0);
        // assert(s1 == ????);
        // assert(s1.getLength() == ????);

        assert(s2.getChars() != nullptr);
        assert(std::strcmp(s2.getChars(), "hello") == 0);
        assert(s2 == "hello");
        assert(s2.getLength() == 5);
    }

    {
        String s("hello");
        s = "gutenmorgen";

        assert(s.getChars() != nullptr);
        assert(std::strcmp(s.getChars(), "gutenmorgen") == 0);
        assert(s == "gutenmorgen");
        assert(s.getLength() == 11);
    }

    {
        String s1("hello");
        String s2;
        s2 = s1;

        assert(s1.getChars() != nullptr);
        assert(std::strcmp(s1.getChars(), "hello") == 0);
        assert(s1 == "hello");
        assert(s1.getLength() == 5);

        assert(s2.getChars() != nullptr);
        assert(std::strcmp(s2.getChars(), "hello") == 0);
        assert(s2 == "hello");
        assert(s2.getLength() == 5);
    }

    {
        String s1("hello");
        String s2;
        s2 = std::move(s1);

        // Testing a moved-from object is tricky
        assert(s1.getChars() != nullptr);
        assert(s1.getChars()[0] == 0);
        // assert(s1 == ????);
        // assert(s1.getLength() == ????);

        assert(s2.getChars() != nullptr);
        assert(std::strcmp(s2.getChars(), "hello") == 0);
        assert(s2 == "hello");
        assert(s2.getLength() == 5);
    }

    {
        String s1("hallo");
        String s2("hello");

        assert(!(s1 == s2));
        assert(s1 != s2);
        assert(s1 < s2);
        assert(!(s2 < s1));
    }

    {
        String s1("hell");
        String s2("hello");

        assert(!(s1 == s2));
        assert(s1 != s2);
        assert(s1 < s2);
        assert(!(s2 < s1));
    }

    {
        String s1("hello");
        String s2("hello");

        assert(s1 == s2);
        assert(!(s1 != s2));
        assert(!(s1 < s2));
        assert(!(s2 < s1));
    }

    {
        String s1;
        String s2;

        assert(s1 == s2);
        assert(!(s1 != s2));
        assert(!(s1 < s2));
        assert(!(s2 < s1));
    }
}
