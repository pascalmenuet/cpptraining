
#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <memory>
#include <functional>

class Shape
{
public:

    virtual ~Shape() = default;

    virtual void draw() const = 0;
};

class Square : public Shape
{
    virtual void draw() const override
    {
        std::cout << "Drawing Square\n";
    }
};

class Triangle : public Shape
{
    virtual void draw() const override
    {
        std::cout << "Drawing Triangle\n";
    }
};

class Circle : public Shape
{
    virtual void draw() const override
    {
        std::cout << "Drawing Circle\n";
    }
};

using ShapePtr = std::unique_ptr<Shape>;

class ShapeFactory
{
public:

    ShapeFactory()
    {
        m_creators.emplace("Square", []() { return std::make_unique<Square>(); });
        m_creators.emplace("Triangle", []() { return std::make_unique<Triangle>(); });
        m_creators.emplace("Circle", []() { return std::make_unique<Circle>(); });
    }

    ShapePtr createShape(const std::string& shapeType) const
    {
        const auto creatorIterator = m_creators.find(shapeType);
        if (creatorIterator == m_creators.end())
            return ShapePtr{};
        const auto creator = creatorIterator->second;
        return creator();
    }

private:

    std::map<std::string, std::function<ShapePtr()>> m_creators;
};

class ShapeManager
{
public:

    void menuLoop()
    {
        std::string command;
        do
        {
            std::cout << "Choose a command\n";
            std::cout << "Type 'c' + ENTER for creating a shape\n";
            std::cout << "Type 'd' + ENTER for displaying the list of shapes\n";
            std::cout << "Type 'q' + ENTER for leaving\n";
            std::cout << "Choice ? ";
            std::cin >> command;

            if (command == "c")
                createShape();
            else if (command == "d")
                drawShapes();

        } while (command != "q");
    }

private:

    void createShape()
    {
        std::string shapeType;
        std::cout << "Which shape type would you like to create ? ";
        std::cin >> shapeType;

        auto upShape = m_shapeFactory.createShape(shapeType);
        if (!upShape)
            std::cout << "Unknown shape type\n";
        else
        {
            std::cout << "New shape created\n";
            m_shapes.push_back(std::move(upShape));
        }
    }

    void drawShapes()
    {
        std::cout << "List of shapes:\n";

        for (const auto& upShape : m_shapes)
            upShape->draw();
    }

    std::vector<ShapePtr> m_shapes;

    ShapeFactory m_shapeFactory;
};

void test_shapes()
{
    ShapeManager shapeManager;
    shapeManager.menuLoop();
}
