
#include "../bookdb/book_database.hpp"
#include "../catch/catch.hpp"
#include <algorithm>


namespace books { namespace ut {

    SCENARIO("We can manage authors in the book database", "[database]")
    {

        GIVEN("an empty book database")
        {
            books::BookDatabase bookDatabase{};
            REQUIRE(bookDatabase.getNumberOfAuthors() == 0);

            WHEN("I add some authors with different names")
            {
                auto result = bookDatabase.addAuthor(Author{ "Scott", "Meyers", 50 });
                REQUIRE(result);
                result = bookDatabase.addAuthor(Author{ "Bjarne", "Stroustrup", 70 });
                REQUIRE(result);
                result = bookDatabase.addAuthor(Author{ "Bjarne", "Proutrup", 70 });
                REQUIRE(result);
                result = bookDatabase.addAuthor(Author{ "Herb", "Sutter", 45 });
                REQUIRE(result);
                result = bookDatabase.addAuthor(Author{ "Andrei", "Alexandrescu", 45 });
                REQUIRE(result);
                REQUIRE(bookDatabase.getNumberOfAuthors() == 5);

                AND_WHEN("I search for an existing author")
                {
                    const auto pAuthor = bookDatabase.findAuthorByFirstNameLastName("Herb", "Sutter");
                    THEN("I find it")
                    {
                        REQUIRE(pAuthor);
                    }
                }

                AND_WHEN("I search for all the authors with the same first name")
                {
                    auto authors = bookDatabase.findAuthorByFirstNameLastNamesByFirstName("Bjarne");
                    THEN("I find them")
                    {
                        REQUIRE(authors.size() == 2);
                        std::sort(authors.begin(), authors.end(), [](const auto& a1, const auto& a2)
                        {
                            return a1->getLastName() < a2->getLastName();
                        });
                        {
                            const auto pAuthor = authors[0];
                            REQUIRE(pAuthor->getFirstName() == "Bjarne");
                            REQUIRE(pAuthor->getLastName() == "Proutrup");
                        }
                        {
                            const auto pAuthor = authors[1];
                            REQUIRE(pAuthor->getFirstName() == "Bjarne");
                            REQUIRE(pAuthor->getLastName() == "Stroustrup");
                        }
                    }
                }

                AND_WHEN("I search for all the authors with the same non-exsiting first name")
                {
                    auto authors = bookDatabase.findAuthorByFirstNameLastNamesByFirstName("Harb");
                    THEN("I get 0 results")
                    {
                        REQUIRE(authors.empty());
                    }
                }

                AND_WHEN("I search for a non-existing author")
                {
                    const auto pAuthor = bookDatabase.findAuthorByFirstNameLastName("Herb", "Potter");
                    THEN("I don't find it")
                    {
                        REQUIRE(!pAuthor);
                    }
                }

                AND_WHEN("I add an already existing author")
                {
                    auto result = bookDatabase.addAuthor(Author{ "Scott", "Meyers", 153 });
                    THEN("I get an error")
                    {
                        REQUIRE(!result);
                    }
                }

                AND_WHEN("I remove an existing author")
                {
                    const auto result = bookDatabase.removeAuthor("Herb", "Sutter");
                    THEN("it succeeds and the number of authors is decremented")
                    {
                        REQUIRE(result);
                        REQUIRE(bookDatabase.getNumberOfAuthors() == 4);
                    }
                }

                AND_WHEN("I remove an non-existing author")
                {
                    const auto result = bookDatabase.removeAuthor("Harb", "Sutter");
                    THEN("it fails and the number of authors does not change")
                    {
                        REQUIRE(!result);
                        REQUIRE(bookDatabase.getNumberOfAuthors() == 5);
                    }
                }
            }
        }

    }

    SCENARIO("We can manage books in the book database", "[database]")
    {

        GIVEN("a book database with some authors")
        {
            books::BookDatabase bookDatabase{};
            bookDatabase.addAuthor(Author{ "Scott", "Meyers", 50 });
            bookDatabase.addAuthor(Author{ "Bjarne", "Stroustrup", 70 });
            bookDatabase.addAuthor(Author{ "Herb", "Sutter", 45 });
            bookDatabase.addAuthor(Author{ "Andrei", "Alexandrescu", 45 });
            const auto pScott = bookDatabase.findAuthorByFirstNameLastName("Scott", "Meyers");
            REQUIRE(pScott);
            const auto pAndrei = bookDatabase.findAuthorByFirstNameLastName("Andrei", "Alexandrescu");
            REQUIRE(pAndrei);
            REQUIRE(bookDatabase.getNumberOfBooks() == 0);

            THEN("I can add a book")
            {
                const auto result = bookDatabase.addBook(Book{ "ABC", "azsdfasd", *pScott, 3205 });
                REQUIRE(result);
                REQUIRE(bookDatabase.getNumberOfBooks() == 1);

                AND_WHEN("I try adding another book with the same uid")
                {
                    const auto resultOther = bookDatabase.addBook(Book{ "ABC", "qwe", *pScott, 789 });
                    THEN("It fails")
                    {
                        REQUIRE(!resultOther);
                        REQUIRE(bookDatabase.getNumberOfBooks() == 1);
                    }
                }

                AND_WHEN("I add a book with another uid")
                {
                    const auto resultOther = bookDatabase.addBook(Book{ "BOO", "qwe", *pScott, 789 });
                    THEN("It succeeds")
                    {
                        REQUIRE(resultOther);
                        REQUIRE(bookDatabase.getNumberOfBooks() == 2);
                    }
                }
            }

            WHEN("I add a book")
            {
                bookDatabase.addBook(Book{ "BOO", "qweqwe", *pScott, 789 });
                REQUIRE(bookDatabase.getNumberOfBooks() == 1);

                THEN("I can find the book")
                {
                    const auto pBook = bookDatabase.findBookByUid("BOO");
                    REQUIRE(pBook != nullptr);
                    REQUIRE(pBook->getUid() == "BOO");
                }

                AND_WHEN("I remove the book")
                {
                    const auto result = bookDatabase.removeBook("BOO");
                    REQUIRE(result);
                    REQUIRE(bookDatabase.getNumberOfBooks() == 0);

                    THEN("I cannot find the book anymore")
                    {
                        const auto pBook = bookDatabase.findBookByUid("BOO");
                        REQUIRE(pBook == nullptr);
                    }
                }
            }

            WHEN("I search for an unknown book")
            {
                const auto pBook = bookDatabase.findBookByUid("BOO");

                THEN("I get a nullptr")
                {
                    REQUIRE(pBook == nullptr);
                }
            }
        }

    }

} } // namespace books::ut
