
#include "../bookdb/book_database.hpp"
#include "../catch/catch.hpp"
#include <algorithm>
#include <array>


namespace books { namespace ut {

    static unsigned int fillDatabaseWithSomeAuthors(BookDatabase& bookDatabase)
    {
        const std::array<Author, 5> someAuthors =
        {
            Author{ "Scott", "Meyers", 50 },
            Author{ "Bjarne", "Stroustrup", 70 },
            Author{ "Bjarne", "Proutrup", 70 },
            Author{ "Herb", "Sutter", 45 },
            Author{ "Andrei", "Alexandrescu", 45 },
        };

        std::for_each(begin(someAuthors), end(someAuthors), [&](const auto& author)
        {
            bookDatabase.addAuthor(author);
        });

        const auto numberOfAuthors = bookDatabase.getNumberOfAuthors();
        REQUIRE(numberOfAuthors == static_cast<unsigned int>(someAuthors.size()));
        return numberOfAuthors;
    }

    static unsigned int fillDatabaseWithSomeBooks(BookDatabase& bookDatabase)
    {
        const auto pScott = bookDatabase.findAuthorByFirstNameLastName("Scott", "Meyers");
        const auto pAndrei = bookDatabase.findAuthorByFirstNameLastName("Andrei", "Alexandrescu");
        const std::array<Book, 4> someBooks =
        {
            Book{ "AAAA", "C++ is great", *pScott, 123 },
            Book{ "BBBB", "C++ sucks", *pAndrei, 234 },
            Book{ "CCCC", "Whats is C++ ?", *pScott, 456 },
            Book{ "DDDD", "C--", *pAndrei, 567 },
        };

        std::for_each(begin(someBooks), end(someBooks), [&](const auto& book)
        {
            bookDatabase.addBook(book);
        });

        const auto numberOfBooks = bookDatabase.getNumberOfBooks();
        REQUIRE(numberOfBooks == static_cast<unsigned int>(someBooks.size()));
        return numberOfBooks;
    }

    TEST_CASE("Manage authors", "[authors]")
    {
        // ARRANGE
        BookDatabase bookDatabase{};
        const auto initialNumberOfAuthors = fillDatabaseWithSomeAuthors(bookDatabase);

        SECTION("Add an author with a non-existing first name into the database")
        {
            // ACT
            const auto result = bookDatabase.addAuthor(Author{ "Scott", "Maya", 12 });

            // ASSERT
            REQUIRE(result);
            REQUIRE(bookDatabase.getNumberOfAuthors() == initialNumberOfAuthors + 1);
            const auto pAuthor = bookDatabase.findAuthorByFirstNameLastName("Scott", "Maya");
            REQUIRE(pAuthor);
        }

        SECTION("Add an author with a non-existing last name into the database")
        {
            // ACT
            const auto result = bookDatabase.addAuthor(Author{ "Arthur", "Alexandrescu", 12 });

            // ASSERT
            REQUIRE(result);
            REQUIRE(bookDatabase.getNumberOfAuthors() == initialNumberOfAuthors + 1);
            const auto pAuthor = bookDatabase.findAuthorByFirstNameLastName("Arthur", "Alexandrescu");
            REQUIRE(pAuthor);
        }

        SECTION("Try adding an author with an already existing pair (first name, last name) into the database")
        {
            // ACT
            const auto result = bookDatabase.addAuthor(Author{ "Scott", "Meyers", 65 });

            // ASSERT
            REQUIRE(!result);
            REQUIRE(bookDatabase.getNumberOfAuthors() == initialNumberOfAuthors);
        }

        SECTION("Remove an existing author from the database")
        {
            // ACT
            const auto result = bookDatabase.removeAuthor("Bjarne", "Stroustrup");

            // ASSERT
            REQUIRE(result);
            REQUIRE(bookDatabase.getNumberOfAuthors() == initialNumberOfAuthors - 1);
            const auto pAuthor = bookDatabase.findAuthorByFirstNameLastName("Bjarne", "Stroustrup");
            REQUIRE(!pAuthor);
        }

        SECTION("Try removing a non-existing author from the database")
        {
            // ACT
            const auto result = bookDatabase.removeAuthor("Bjarne", "Floutch");

            // ASSERT
            REQUIRE(!result);
            REQUIRE(bookDatabase.getNumberOfAuthors() == initialNumberOfAuthors);
        }

        SECTION("Try removing am existing author who has some active books from the database")
        {
            // ARRANGE
            fillDatabaseWithSomeBooks(bookDatabase);

            // ACT
            const auto result = bookDatabase.removeAuthor("Andrei", "Alexandrescu");

            // ASSERT
            REQUIRE(!result);
            REQUIRE(bookDatabase.getNumberOfAuthors() == initialNumberOfAuthors);
        }

        SECTION("Find an existing author in the database")
        {
            // ACT
            const auto pAuthor = bookDatabase.findAuthorByFirstNameLastName("Bjarne", "Stroustrup");

            // ASSERT
            REQUIRE(pAuthor);
            REQUIRE(pAuthor->getFirstName() == "Bjarne");
            REQUIRE(pAuthor->getLastName() == "Stroustrup");
            REQUIRE(pAuthor->getAge() == 70);
        }

        SECTION("Try finding an non-existing author in the database")
        {
            // ACT
            const auto pAuthor = bookDatabase.findAuthorByFirstNameLastName("Paul", "Bismuth");

            // ASSERT
            REQUIRE(!pAuthor);
        }

        SECTION("Find existing authors with the same first name in the database")
        {
            // ACT
            auto authors = bookDatabase.findAuthorByFirstNameLastNamesByFirstName("Bjarne");

            // ASSERT
            REQUIRE(authors.size() == 2);
            std::sort(authors.begin(), authors.end(), [](const auto& a1, const auto& a2)
            {
                return a1->getLastName() < a2->getLastName();
            });
            {
                const auto pAuthor = authors[0];
                REQUIRE(pAuthor->getFirstName() == "Bjarne");
                REQUIRE(pAuthor->getLastName() == "Proutrup");
            }
            {
                const auto pAuthor = authors[1];
                REQUIRE(pAuthor->getFirstName() == "Bjarne");
                REQUIRE(pAuthor->getLastName() == "Stroustrup");
            }
        }

        SECTION("Try finding non-existing authors with the same first name in the database")
        {
            // ACT
            const auto authors = bookDatabase.findAuthorByFirstNameLastNamesByFirstName("Angela");

            // ASSERT
            REQUIRE(authors.empty());
        }
    }

    TEST_CASE("Manage books", "[books]")
    {
        // ARRANGE
        BookDatabase bookDatabase{};
        const auto initialNumberOfAuthors = fillDatabaseWithSomeAuthors(bookDatabase);
        const auto initialNumberOfBooks = fillDatabaseWithSomeBooks(bookDatabase);

        SECTION("Add a book with a non-existing uid into the database")
        {
            // ACT
            const auto result = bookDatabase.addBook(Book{ "EEEE", "Something", *bookDatabase.findAuthorByFirstNameLastName("Herb", "Sutter"), 654 });

            // ASSERT
            REQUIRE(result);
            REQUIRE(bookDatabase.getNumberOfBooks() == initialNumberOfBooks + 1);
            const auto pBook = bookDatabase.findBookByUid("EEEE");
            REQUIRE(pBook);
        }

        SECTION("Try adding a book with an existing uid into the database")
        {
            // ACT
            const auto result = bookDatabase.addBook(Book{ "BBBB", "Something", *bookDatabase.findAuthorByFirstNameLastName("Herb", "Sutter"), 654 });

            // ASSERT
            REQUIRE(!result);
            REQUIRE(bookDatabase.getNumberOfBooks() == initialNumberOfBooks);
        }

        SECTION("Remove an existing book from the database")
        {
            // ACT
            const auto result = bookDatabase.removeBook("CCCC");

            // ASSERT
            REQUIRE(result);
            REQUIRE(bookDatabase.getNumberOfBooks() == initialNumberOfBooks - 1);
            const auto pBook = bookDatabase.findBookByUid("CCCC");
            REQUIRE(!pBook);
        }

        SECTION("Try removing a non-existing book from the database")
        {
            // ACT
            const auto result = bookDatabase.removeBook("FFFF");

            // ASSERT
            REQUIRE(!result);
            REQUIRE(bookDatabase.getNumberOfBooks() == initialNumberOfBooks);
        }

        SECTION("Find an existing book in the database")
        {
            // ACT
            const auto pBook = bookDatabase.findBookByUid("AAAA");

            // ASSERT
            REQUIRE(pBook);
            REQUIRE(pBook->getUid() == "AAAA");
        }

        SECTION("Try finding a non-existing book in the database")
        {
            // ACT
            const auto pBook = bookDatabase.findBookByUid("ZZZZ");

            // ASSERT
            REQUIRE(!pBook);
        }

        SECTION("Find all the books of an existing author in the database")
        {
            // ACT
            auto books = bookDatabase.findBooksByAuthor("Scott", "Meyers");

            // ASSERT
            REQUIRE(books.size() == 2);
            std::sort(books.begin(), books.end(), [](const auto& b1, const auto& b2)
            {
                return b1->getUid() < b2->getUid();
            });
            {
                const auto pBook = books[0];
                REQUIRE(pBook->getUid() == "AAAA");
            }
            {
                const auto pBook = books[1];
                REQUIRE(pBook->getUid() == "CCCC");
            }
        }
    }

} } // namespace books::ut
