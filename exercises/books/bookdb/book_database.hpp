
#pragma once


#include "book.hpp"
#include "author.hpp"
#include <unordered_map>
#include <set>


namespace books {

    class BookDatabase
    {
    public:

        unsigned int getNumberOfBooks() const;

        bool addBook(const Book& book);

        const Book* findBookByUid(const std::string& uid) const;

        std::vector<const Book*> findBooksByAuthor(const std::string& firstName, const std::string& lastName) const;

        bool removeBook(const std::string& uid);

        unsigned int getNumberOfAuthors() const;

        bool addAuthor(const Author& author);

        const Author* findAuthorByFirstNameLastName(const std::string& firstName, const std::string& lastName) const;

        std::vector<const Author*> findAuthorByFirstNameLastNamesByFirstName(const std::string& firstName) const;

        bool removeAuthor(const std::string& firstName, const std::string& lastName);

    private:

        using UMapBooks = std::unordered_map<std::string, Book>;
        using USetAuthors = std::set<Author>;

        UMapBooks m_books;
        USetAuthors m_authors;
    };

} // namespace books
