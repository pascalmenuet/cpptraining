
#include "author.hpp"


namespace books {

    Author::Author(
        const std::string& firstName,
        const std::string& lastName,
        unsigned int age
        )
        : m_firstName(firstName)
        , m_lastName(lastName)
        , m_age(age)
    {
    }

} // namespace books
