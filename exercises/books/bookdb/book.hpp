
#pragma once


#include <string>


namespace books {

    class Author;

    class Book
    {
    public:

        Book(
            const std::string& uid,
            const std::string& title,
            const Author& author,
            unsigned int numberOfPages
            );

        const std::string& getUid() const;

        const Author& getAuthor() const
        {
            return m_author;
        }

    private:

        std::string m_uid;
        std::string m_title;
        const Author& m_author;
        unsigned int m_numberOfPages = 0;
    };

} // namespace books
