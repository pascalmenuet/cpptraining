
#pragma once


#include <string>


namespace books {

    class Author
    {
    public:

        Author(
            const std::string& firstName,
            const std::string& lastName,
            unsigned int age
            );

        const std::string& getFirstName() const
        {
            return m_firstName;
        }

        const std::string& getLastName() const
        {
            return m_lastName;
        }

        unsigned int Author::getAge() const
        {
            return m_age;
        }

    private:

        std::string m_firstName;
        std::string m_lastName;
        unsigned int m_age = 0;
    };

    inline bool operator<(const Author& author1, const Author& author2)
    {
        if (author1.getFirstName() < author2.getFirstName())
            return true;
        if (author1.getFirstName() > author2.getFirstName())
            return false;
        return author1.getLastName() < author2.getLastName();
    }

} // namespace books
