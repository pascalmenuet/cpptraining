
#include "book.hpp"


namespace books {

    Book::Book(
        const std::string& uid,
        const std::string& title,
        const Author& author,
        unsigned int numberOfPages
        )
        : m_uid(uid)
        , m_title(title)
        , m_author(author)
        , m_numberOfPages(numberOfPages)
    {
    }

    const std::string& Book::getUid() const
    {
        return m_uid;
    }

} // namespace books
