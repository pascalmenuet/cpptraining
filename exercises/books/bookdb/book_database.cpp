
#include "book_database.hpp"
#include <iterator>
#include <algorithm>


namespace books {

    unsigned int BookDatabase::getNumberOfBooks() const
    {
        return static_cast<unsigned int>(m_books.size());
    }

    bool BookDatabase::addBook(const Book& book)
    {
        const auto result = m_books.emplace(book.getUid(), book);
        return result.second;
    }

    const Book* BookDatabase::findBookByUid(const std::string& uid) const
    {
        const auto iterBook = m_books.find(uid);
        if (iterBook == m_books.end())
            return nullptr;
        const auto pBook = &iterBook->second;
        return pBook;
    }

    std::vector<const Book*> BookDatabase::findBooksByAuthor(const std::string& firstName, const std::string& lastName) const
    {
        std::vector<const Book*> foundBooks;
        for (const auto& book : m_books)
        {
            const auto& bookAuthor = book.second.getAuthor();
            if (bookAuthor.getFirstName() == firstName && bookAuthor.getLastName() == lastName)
                foundBooks.push_back(&book.second);
        }
        return foundBooks;
    }

    bool BookDatabase::removeBook(const std::string& uid)
    {
        const auto iterBook = m_books.find(uid);
        if (iterBook == m_books.end())
            return false;
        m_books.erase(iterBook);
        return true;
    }

    unsigned int BookDatabase::getNumberOfAuthors() const
    {
        return static_cast<unsigned int>(m_authors.size());
    }

    bool BookDatabase::addAuthor(const Author& author)
    {
        const auto result = m_authors.emplace(author);
        return result.second;
    }

    const Author* BookDatabase::findAuthorByFirstNameLastName(const std::string& firstName, const std::string& lastName) const
    {
        const auto iterAuthor = m_authors.find(Author{firstName, lastName, 0});
        if (iterAuthor == m_authors.end())
            return nullptr;
        const auto pAuthor = &*iterAuthor;
        return pAuthor;
    }

    std::vector<const Author*> BookDatabase::findAuthorByFirstNameLastNamesByFirstName(const std::string& firstName) const
    {
        std::vector<const Author*> foundAuthors;
        const auto iterAuthorFirst = m_authors.lower_bound(Author{ firstName, std::string(), 0 });
        const auto iterAuthorEnd = std::find_if(iterAuthorFirst, m_authors.end(), [&firstName](const auto& author)
        {
            return author.getFirstName() != firstName;
        });
        std::transform(iterAuthorFirst, iterAuthorEnd, std::back_inserter(foundAuthors), [](const auto& author)
        {
            return &author;
        });
        return foundAuthors;
    }

    bool BookDatabase::removeAuthor(const std::string& firstName, const std::string& lastName)
    {
        const auto iterAuthor = m_authors.find(Author{ firstName, lastName, 0 });
        if (iterAuthor == m_authors.end())
            return false;
        const auto& authorToDelete = *iterAuthor;
        const auto hasThisAuthorWrittenSomeBook = std::any_of(m_books.begin(), m_books.end(), [&authorToDelete](const auto& book)
        {
            return &book.second.getAuthor() == &authorToDelete;
        });
        if (hasThisAuthorWrittenSomeBook)
            return false;
        m_authors.erase(iterAuthor);
        return true;
    }

} // namespace books
