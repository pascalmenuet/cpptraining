

// class Integer

// You can default construct an Integer: this set its value to 0
// You can copy-construct it 
// You can copy-assign it 
// You can construct it from an int
// You can assign it from an int
// You can compare two Integers with operator ==
// You can order two Integers with operator <
// You can convert an Integer to an int

#include <cassert>

class Integer
{
public:

    Integer() = default;

    Integer(int i)
        : m_i(i)
    {
    }

    Integer(const Integer&) = default;

    Integer& operator=(const Integer&) = default;

    operator int() const
    {
        return m_i;
    }

    int operator++(int)
    {
        const auto i = m_i;
        ++m_i;
        return i;
    }

private:

    int m_i;
};

int main()
{
    Integer i = 6;
    assert(i == 6);
    i++;
    assert(i == 7);

    return 0;
}
