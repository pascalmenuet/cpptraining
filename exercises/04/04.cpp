
#include <iostream>

int compute(int param)
{
    int result;
    if (param < 0)
        result = param * param;
    else if (param > 0)
        result = param + param;
    return result;
}

int main()
{
    std::cout << "Enter a param: ";
    int param;
    std::cin >> param;

    const auto result = compute(param);

    std::cout << "Result is: " << result << "\n";
    return 0;
}
